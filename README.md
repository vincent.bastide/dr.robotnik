# Dr.Robotnik

## Installation

__(Python virtual environment, Node.JS and NPM are required)__

Clone the repo : 

`$ git clone https://gitlab.com/vincent.bastide/dr.robotnik.git`

Then at the directory root, create a Python virtual environment and activate it :

```
$ python3 -m venv .env
$ . .env/bin/activate
```

Now that the virtual env. is ready, you need to install dependencies :

`$ pip install -r requirements.txt`

You can cd into the app folder and start flask :

`$ cd app && python -m flask run`

Now you can access the app on localhost:5000

__If you are in development mode use the following commands__

At the root of the git repo :

```
$ npm i
$ npm run start:dev
```

Now you can access the app on localhost:9000
