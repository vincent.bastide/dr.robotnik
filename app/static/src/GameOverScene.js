import Phaser from 'phaser';

export default class GameOverScene extends Phaser.Scene {
    constructor() {
        super({ key: "GameOverScene" });
    }

    init(data) {
        this.score = data.score;
    }

    preload() {}

    create() {
        const style = {
            font: "16px Monospace",
            fill: "#FFF",
            align: "center"
        };

        this.add.text(
            this.sys.game.canvas.width / 8,
            this.sys.game.canvas.height / 2,
            `Game Over\n\nScore : ${this.score}\n\nAppuyer sur une touche\npour relancer.`,
            style
        );
        this.input.keyboard.on(
            'keydown',
            () => this.scene.start('MainGameScene'),
            this
        );
    }
}
