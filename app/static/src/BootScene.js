import 'phaser'

export default class BootScene extends Phaser.Scene {
    constructor() {
        super({ key: 'bootScene' });
    } 

    preload() {}

    create() {
        const style = {
            font: "16px Monospace",
            fill: "#FFF",
            align: "center"
        };
        this.add.text(
            this.sys.game.canvas.width / 6,
            this.sys.game.canvas.height / 2,
            'Press Enter\nto start the game',
            style
        );

        this.input.keyboard.on(
            'keydown-ENTER',
            () => this.scene.start('MainGameScene'),
            this
        );
    }
}
