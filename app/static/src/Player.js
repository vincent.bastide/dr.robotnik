const BlockPositions = Object.freeze({
    ABOVE: 0,
    RIGHT: 1,
    BELOW: 2,
    LEFT: 3
});

const moveBlockTo = Object.freeze({
    0: { x: 0, y: -30 },
    1: { x: 30, y: 0 },
    2: { x: 0, y: 30 },
    3: { x: -30, y: 0 }
});

export class Player {
    constructor({ block_1, block_2 }) {
        this.firstBlock = block_1;
        this.secondBlock = block_2;
        this.secondBlockPos = BlockPositions.ABOVE;
        this._isPlaced = false;
        this.x = 4;
        this.y = 0;
    }

    get position() {
        return { x: this.firstBlock.x, y: this.firstBlock.y };
    }

    get height() {
        //return this.secondBlockPos > 0 ? 30 : 60;
        return this.secondBlockPos == 2 ? 60 : 30;
    }

    get width() {
        //return this.secondBlockPos < 2 ? 30 : 60;
        return [BlockPositions.LEFT, BlockPositions.RIGHT].indexOf(this.secondBlockPos) > -1 ? 60 : 30;
    }

    get isPlaced() {
        return this._isPlaced;
    }

    setIsPlaced(isPlaced) {
        this._isPlaced = isPlaced;
    }

    canMoveDown() {
        const threshold = this.secondBlockPos == BlockPositions.BELOW ? 510 : 540;
        if (this.height + this.position.y > threshold + this.height) {
            this.setIsPlaced(true);
            return false;
        }
        return true;
    }

    canMoveRight() {
        const threshold = this.secondBlockPos == BlockPositions.RIGHT ? 180 : 210;
        if (this.width + this.position.x > threshold + this.width) {
            return false;
        }
        return true;
    }

    canMoveLeft() {
        const threshold = this.secondBlockPos == BlockPositions.LEFT ? 30 : 30;
        if (this.width + this.position.x < threshold + this.width) {
            return false;
        }
        return true;
    }

    moveDown() {
        this.firstBlock.y += 30;
        this.secondBlock.y += 30;
    }

    moveRight() {
        this.firstBlock.x += 30;
        this.secondBlock.x += 30;
    }

    moveLeft() {
        this.firstBlock.x -= 30;
        this.secondBlock.x -= 30;
    }

    changeSecondblockPosition() {
        let nextPosition = this.secondBlockPos < 3 ? this.secondBlockPos + 1 : 0; 
        console.log(nextPosition);
        console.log(moveBlockTo[nextPosition].x);
        console.log(moveBlockTo[nextPosition].y);
        this.secondBlock.x = this.firstBlock.x + moveBlockTo[nextPosition].x
        this.secondBlock.y = this.firstBlock.y + moveBlockTo[nextPosition].y
        this.secondBlockPos = nextPosition;
    }
}
