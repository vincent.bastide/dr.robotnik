import Phaser from 'phaser';
import BootScene from './BootScene';
import MainGameScene from './MainGameScene';
import GameOverScene from './GameOverScene';

const config = {
    type: Phaser.AUTO,
    width: 270,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            debug: true,
            gravity: { y: 200 },
        }
    },
    scene: [
        BootScene,
        MainGameScene,
        GameOverScene
    ],
    fps: {
        target: 60,
        forceSetTimeOut: true
    }
};

const game = new Phaser.Game(config);
