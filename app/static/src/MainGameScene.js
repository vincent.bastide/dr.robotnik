import Phaser from 'phaser';
import { Board } from './Board';
import { Player } from './Player';

function generateBlocks(mainState) {
    let block_1 = mainState.add.sprite(4 * 30, -30, 'block').setOrigin(0, 0);
    let block_2 = mainState.add.sprite(4 * 30, block_1.y - 30, 'block').setOrigin(0, 0);
    block_1.setFrame(Math.floor(Math.random() * 4) + 1);
    block_2.setFrame(Math.floor(Math.random() * 4) + 1);
    return { block_1, block_2 };
}

function clearGrid(grid) {
    grid = grid[0].map((_, colIndex) => grid.map(row => row[colIndex]));
    for (let i = grid.length - 1; i > 0; --i) {
        let tmpArray = grid[i].filter(el => el != null);
        grid[i] = [...Array(grid[0].length - tmpArray.length).fill(null), ...tmpArray];
    }
    grid = grid[0].map((_, colIndex) => grid.map(row => row[colIndex]));
    for (let i = grid.length - 1; i > 0; --i) {
        for (let j = 0; j < grid[0].length; ++j) {
            if (grid[i][j] != null) {
                grid[i][j].x = j * 30;
                grid[i][j].y = i * 30;
            } 
        }
    }
    return grid;
}

export default class MainGameScene extends Phaser.Scene {
    constructor() {
        super({ key: "MainGameScene" });
        this.player = undefined;
        this.board = new Board();
        this.cursors = undefined; 
        this.scoreText = "";
        this.score = 0;
    }

    preload() {
        this.cursors = this.input.keyboard.createCursorKeys();
        this.load.spritesheet('block', 'assets/blocks.png', {
            frameWidth: 30, frameHeight: 30
        });
        this.load.image('rock', 'assets/rock.png')
    }

    create() {
        this.board.reset();
        this.player = new Player(generateBlocks(this));

        this.events.on('shutdown', this.shutdown, this);
        this.time.addEvent({ delay: 250, callback: () => { 
            const coordinates = this.player.position;
            if (
                this.player.canMoveDown() &&
                this.board.grid[(coordinates.y + this.player.height) / 30][coordinates.x / 30] === null
            ) {
                this.player.moveDown()
            } else this.player.setIsPlaced(true); 
        }, callbackScope: this, loop: true });

        this.scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '16px', fill: '#FFF' });
    }

    update() {
        if (!this.player.isPlaced) {
            if (this.input.keyboard.checkDown(this.cursors.up, 250)) {
                this.player.changeSecondblockPosition();
            } 
            if (
                this.input.keyboard.checkDown(this.cursors.left, 250) &&
                this.player.canMoveLeft() &&
                this.board.grid[this.player.position.y / 30][(this.player.position.x - ((this.player.secondBlockPos == 1) ? this.player.width / 2 : this.player.width)) / 30] === null
            ) {
                this.player.moveLeft();
            } else if (
                this.input.keyboard.checkDown(this.cursors.right, 250) &&
                this.player.canMoveRight() &&
                this.board.grid[this.player.position.y / 30][(this.player.position.x + ((this.player.secondBlockPos == 3) ? this.player.width / 2 : this.player.width)) / 30] === null
            ) {
                this.player.moveRight();
            }

        } else {
            try {
                this.board.grid[this.player.firstBlock.y / 30][this.player.firstBlock.x / 30] = this.player.firstBlock;
                this.board.grid[this.player.secondBlock.y / 30][this.player.secondBlock.x / 30] = this.player.secondBlock;

                this.checkNeighbors(
                    {x: new Set(), y: new Set()},
                    this.player.firstBlock.frame.name,
                    { y: this.player.firstBlock.y / 30, x: this.player.firstBlock.x / 30 },
                    1
                );
                this.board.grid = clearGrid(this.board.grid);

                this.checkNeighbors(
                    {x: new Set(), y: new Set()},
                    this.player.secondBlock.frame.name,
                    { y: this.player.secondBlock.y / 30, x: this.player.secondBlock.x / 30 },
                    1
                );
                this.board.grid = clearGrid(this.board.grid);

                this.player = new Player(generateBlocks(this));
            } catch (e) {
                this.scene.start('GameOverScene', { score: this.score })
            }
        }

    }

    shutdown() {
        this.input.keyboard.shutdown();
    }

    checkNeighbors(history, color, position, count) {
        const leftBlock = { y: position.y, x: position.x - 1 };
        const rightBlock = { y: position.y, x: position.x + 1 };
        const aboveBlock = { y: position.y - 1, x: position.x };
        const belowBlock = { y: position.y + 1, x: position.x };

        history.x.add(position.x);
        history.y.add(position.y);

        if (!history.x.has(rightBlock.x) && this.board.grid[rightBlock.y][rightBlock.x] != null && this.board.grid[rightBlock.y][rightBlock.x].frame.name == color) {
            count++
            count = this.checkNeighbors(history, color, { y: rightBlock.y, x: rightBlock.x }, count);
        }
        else if (!history.x.has(leftBlock.x) && this.board.grid[leftBlock.y][leftBlock.x] != null && this.board.grid[leftBlock.y][leftBlock.x].frame.name == color) {
            count++
            count = this.checkNeighbors(history, color, { y: leftBlock.y, x: leftBlock.x }, count);
        }
        else if (!history.y.has(aboveBlock.y) && this.board.grid[aboveBlock.y][aboveBlock.x] != null && this.board.grid[aboveBlock.y][aboveBlock.x].frame.name == color) {
            count++
            count = this.checkNeighbors(history, color, { y: aboveBlock.y, x: aboveBlock.x }, count);
        }
        else if (position.y < 18 && !history.y.has(belowBlock.y) && this.board.grid[belowBlock.y][belowBlock.x] != null && this.board.grid[belowBlock.y][belowBlock.x].frame.name == color) {
            count++
            count = this.checkNeighbors(history, color, { y: belowBlock.y, x: belowBlock.x }, count);
        }

        if (count >= 4) {
            console.log('DELETE');
            this.board.grid[position.y][position.x].destroy();
            this.board.grid[position.y][position.x] = null;
            this.score  += 1;
            this.scoreText.setText(`Score: ${ this.score }`);
        }
        return count;
    }
}


